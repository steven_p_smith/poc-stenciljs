import { newE2EPage } from '@stencil/core/testing';

describe('ds-progress', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<ds-progress></ds-progress>');

    const element = await page.find('ds-progress');
    expect(element).toHaveClass('hydrated');
  });
});
