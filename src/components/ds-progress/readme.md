# ds-progress

<!-- Auto Generated Below -->

## Properties

| Property  | Attribute | Description | Type     | Default |
| --------- | --------- | ----------- | -------- | ------- |
| `current` | `current` |             | `number` | `1`     |
| `total`   | `total`   |             | `number` | `2`     |

---

_Built with [StencilJS](https://stenciljs.com/)_
