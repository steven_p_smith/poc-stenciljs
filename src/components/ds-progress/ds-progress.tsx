import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: "ds-progress",
  styleUrl: "ds-progress.scss",
  shadow: true
})
export class DsProgress {
  @Prop()
  current: number = 1;
  @Prop()
  total: number = 2;

  steps = Array.from({ length: this.total }, (v, i) => {
    return {
      value: v,
      index: i + 1,
      current: i + 1 === this.current
    };
  });

  render() {
    return (
      <div class="ds-progress-wrapper">
        <div class="ds-progress" aria-ignore="true">
          {this.steps.map(step => (
            <div class={step.current ? "step current" : "step"}>
              <div>{step.index}</div>
            </div>
          ))}
        </div>
        <div class="sr-only">
          étape {this.current} de {this.total}
        </div>
      </div>
    );
  }
}
