import { newSpecPage } from '@stencil/core/testing';
import { DsProgress } from './ds-progress';

describe('ds-progress', () => {
  it('builds', () => {
    expect(new DsProgress()).toBeTruthy();
  });


  it('should render default version of button', async () => {
    const page = await newSpecPage({
      components: [DsProgress],
      html: `<ds-progress></ds-progress>`
    });
    expect(page.root).toEqualHtml(`
    <ds-progress>
    <mock:shadow-root>
      <div class="ds-progress-wrapper">
        <div class="ds-progress" aria-ignore="true">
          <div class="step current">
            <div>1</div>
          </div>
          <div class="step">
            <div>2</div>
          </div>
        </div>
        <div class="sr-only">
          étape 1 de 2
        </div>
      </div>
      </mock:shadow-root>
      </ds-progress>
    `);
  });

});
