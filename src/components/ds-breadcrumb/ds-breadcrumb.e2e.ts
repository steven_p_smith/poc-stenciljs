import { newE2EPage } from '@stencil/core/testing';

describe('ds-breadcrumb', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<ds-breadcrumb></ds-breadcrumb>');

    const element = await page.find('ds-breadcrumb');
    expect(element).toHaveClass('hydrated');
  });
});
