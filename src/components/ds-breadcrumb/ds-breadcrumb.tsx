import { Component, Element, h } from "@stencil/core";
@Component({
  tag: "ds-breadcrumb",
  styleUrl: "ds-breadcrumb.scss",
  shadow: true
})
export class DsBreadcrumb {
  @Element() el: HTMLElement;
  private links = Array.from(this.el.querySelectorAll("a"));
  private navLabel =
    document.documentElement.lang === "en" ? "Breadcrumb" : "Fil d'Ariane";
  render() {
    const elements = this.links;
    return (
      <nav aria-label={this.navLabel}>
        <ol>
          {elements.map((value, index) => {
            if (value.className !== "noHref") {
              return (
                <li key={index}>
                  <a href={value.href}>{value.innerText}</a>
                </li>
              );
            } else {
              return (
                <li key={index}>
                  <span class="noHref">{value.innerText}</span>
                </li>
              );
            }
          })}
        </ol>
        <slot></slot>
      </nav>
    );
  }
  componentDidLoad() {
    // delete the slot so oringinal links don't render
    if (this.el.shadowRoot) {
      const tempSlot = this.el.shadowRoot.querySelector("slot");
      if (tempSlot) {
        tempSlot.remove();
      }
    }
  }
}
