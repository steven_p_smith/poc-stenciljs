import { newSpecPage } from '@stencil/core/testing';
import { DsBreadcrumb } from './ds-breadcrumb';

const html = `
<ds-breadcrumb>
  <a href="http://testing.stenciljs.com/1">First</a>
  <a class="noHref">No link</a>
  <a href="http://testing.stenciljs.com/2">Second</a>
  <a class="noHref">Active</a>
</ds-breadcrumb>
`;

const basicOutput = (label) => {
  return `
  <ds-breadcrumb>
    <mock:shadow-root>
      <nav aria-label="${label}">
        <ol>
          <li><a href="http://testing.stenciljs.com/1">First</a></li>
          <li><span class="noHref">No link</span></li>
          <li><a href="http://testing.stenciljs.com/2">Second</a></li>
          <li><span class="noHref">Active</span></li>
        </ol>
      </nav>
    </mock:shadow-root>
    <a href="http://testing.stenciljs.com/1">
      First
    </a>
    <a class="noHref">
      No link
    </a>
    <a href="http://testing.stenciljs.com/2">
      Second
    </a>
    <a class="noHref">
      Active
    </a>
  </ds-breadcrumb>
`
}

describe('ds-breadcrumb', () => {
  it('builds', () => {
    expect(new DsBreadcrumb()).toBeTruthy();
  });


  // validate that render output is what it should be
  it('should render basic version with english aria-label', async () => {
    const page = await newSpecPage({
      components: [DsBreadcrumb],
      html: html,
      language: 'en'
    });
    expect(page.root).toEqualHtml(basicOutput('Breadcrumb'));
  });

  it('should render basic version with french aria-label', async () => {
    const page = await newSpecPage({
      components: [DsBreadcrumb],
      html: html,
      language: 'fr'
    });
    expect(page.root).toEqualHtml(basicOutput('Fil d\'Ariane'));
  });

  it('should render basic version with french aria-label if no language is defined', async () => {
    const page = await newSpecPage({
      components: [DsBreadcrumb],
      html: html
    });
    expect(page.root).toEqualHtml(basicOutput('Fil d\'Ariane'));
  });

  it('should render correct number of links', async () => {
    const page = await newSpecPage({
      components: [DsBreadcrumb],
      html: html
    });
    expect(page.root.shadowRoot.querySelectorAll('a').length).toBe(2);
    expect(page.root.shadowRoot.querySelectorAll('span').length).toBe(2);
    expect(page.root.shadowRoot.querySelectorAll('li').length).toBe(4);
    expect(page.root.shadowRoot.querySelectorAll('nav').length).toBe(1);
  });

  // In theory we can test result when shadowDom is not available, but for some reason returning 6 links here instead of 4
  // TODO fix me
  // it('should render without shadowDom', async () => {
  //   const page = await newSpecPage({
  //     components: [DsBreadcrumb],
  //     html: html,
  //     supportsShadowDom: false
  //   });
  //   expect(page.root.querySelectorAll('a').length).toBe(4);
  // });

});
