# ds-button

<!-- Auto Generated Below -->

## Properties

| Property       | Attribute       | Description | Type                                                                                   | Default     |
| -------------- | --------------- | ----------- | -------------------------------------------------------------------------------------- | ----------- |
| `color`        | `color`         |             | `"error" \| "info" \| "invert" \| "primary" \| "secondary" \| "tertiary" \| "warning"` | `"primary"` |
| `disabled`     | `disabled`      |             | `boolean`                                                                              | `undefined` |
| `iconLeading`  | `icon-leading`  |             | `"round" \| "square"`                                                                  | `null`      |
| `iconTrailing` | `icon-trailing` |             | `"round" \| "square"`                                                                  | `null`      |
| `size`         | `size`          |             | `"default" \| "large" \| "small"`                                                      | `"default"` |
| `type`         | `type`          |             | `"button" \| "reset" \| "submit"`                                                      | `"button"`  |

---

_Built with [StencilJS](https://stenciljs.com/)_
