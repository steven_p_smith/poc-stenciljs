import { Component, h, Prop } from "@stencil/core";
import { CssClassMap } from "../../utils/interfaces";

@Component({
  tag: "ds-button",
  styleUrl: "ds-button.scss"
})
export class Button {
  @Prop({ reflectToAttr: true })
  disabled: boolean;

  @Prop()
  type: "button" | "reset" | "submit" = "button";
  @Prop()
  color:
    | "primary"
    | "invert"
    | "secondary"
    | "tertiary"
    | "info"
    | "warning"
    | "error" = "primary";
  @Prop()
  iconLeading: "square" | "round" = null;
  @Prop()
  iconTrailing: "square" | "round" = null;

  @Prop()
  size: "small" | "default" | "large" = "default";

  render() {
    const classMap = this.getCssClassMap();

    return (
      <button type={this.type} class={classMap} disabled={this.disabled}>
        {!!this.iconLeading ? (
          <svg
            class="icon"
            focusable="false"
            viewBox="0 0 32 32"
            aria-hidden="true"
          >
            <use xlinkHref="/assets/images/icons.svg#fill-alertes-aide" />
          </svg>
        ) : null}

        <slot />
        {!!this.iconTrailing ? (
          <svg
            class="icon"
            focusable="false"
            viewBox="0 0 32 32"
            aria-hidden="true"
          >
            <use xlinkHref="/assets/images/icons.svg#fill-alertes-aide" />
          </svg>
        ) : null}
      </button>
    );
  }

  private getCssClassMap(): CssClassMap {
    return {
      leading: !!this.iconLeading,
      trailing: !!this.iconTrailing,
      [this.color]: true,
      [this.size]: true
    };
  }
}

// net group SR-APP-SIPL-DesjardinsComVPSM-developpeur /domain
// <html lang="fr">

// <head>
//   <meta charset="UTF-8">
//   <meta name="viewport" content="width=device-width, initial-scale=1.0">
//   <meta http-equiv="X-UA-Compatible" content="ie=edge">

//   <title>Desjardins Design System | Bouton primaire</title>

//   <!-- Minified JS -->
//   <script src="/js/main.min.js"></script>
//   <script>svg4everybody(); // run it now or whenever you are ready</script>

//   <link href="../../css/styles.css" rel="stylesheet" />
//   <link href="../../css/dssite/styles.css" rel="stylesheet" />

//   <link href="button.css" rel="stylesheet" />
//   <style rel="stylesheet">
//     button {
//       margin-bottom: 16px;
//     }
//   </style>
// </head>

// <body style="background: white; padding: 30px 20px;">

//   <h1>Bouton primaire - 02.01.01</h1>

//   <h2>Bouton par défaut</h2>
//   <div>
//     <h3>Neutre</h3>
//     <button class="ds-button primary" type="button">Libellé</button>

//     <h3>Icône droite</h3>
//     <button class="ds-button primary icon trailing" type="button">
//       Libellé
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3>Avec icône gauche</h3>
//     <button class="ds-button primary icon leading" type="button">
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//       Libellé
//     </button>

//     <h3>Icône seule</h3>
//     <button class="ds-button primary icon-only" type="button">
//       <span class="sr-only">Libellé</span>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3>Désactivé</h3>
//     <button class="ds-button primary" type="button" tabindex="-1" disabled>
//       Libellé
//     </button>

//     <h3>Désactivé icône droite</h3>
//     <button class="ds-button primary icon trailing" type="button" tabindex="-1" disabled>
//       Libellé
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3>Désactivé avec icône gauche</h3>
//     <button class="ds-button primary icon leading" type="button" tabindex="-1" disabled>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//       Libellé
//     </button>

//     <h3>Désactivé avec icône seule</h3>
//     <button class="ds-button primary icon-only" type="button" tabindex="-1" disabled>
//       <span class="sr-only">Libellé</span>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>
//   </div>

//   <h2>Bouton variante inversée</h2>
//   <div style="background: #383838; padding: 20px;">
//     <h3 style="color: white;">Neutre</h3>
//     <button class="ds-button primary invert" type="button">Libellé</button>

//     <h3 style="color: white;">Avec icône à droite</h3>
//     <button class="ds-button primary invert icon trailing" type="button">
//       Libellé <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3 style="color: white;">Avec icône à gauche</h3>
//     <button class="ds-button primary invert icon leading" type="button">
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg> Libellé
//     </button>

//     <h3 style="color: white;">Icône seule</h3>
//     <button class="ds-button primary invert icon-only" type="button">
//       <span class="sr-only">Libellé</span>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3 style="color: white;">Désactivé</h3>
//     <button class="ds-button primary invert" type="button" tabindex="-1" disabled>
//       Libellé
//     </button>

//     <h3 style="color: white;">Désactivé avec icône à droite</h3>
//     <button class="ds-button primary invert icon trailing" type="button" tabindex="-1" disabled>
//       Libellé <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//     <h3 style="color: white;">Désactivé avec icône à gauche</h3>
//     <button class="ds-button primary invert icon leading" type="button" tabindex="-1" disabled>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg> Libellé
//     </button>

//     <h3 style="color: white;">AA Désactivé icône seule</h3>
//     <button class="ds-button primary invert icon-only" type="button" tabindex="-1" disabled>
//       <span class="sr-only">Libellé</span>
//       <svg class="icon" focusable="false" viewbox="0 0 32 32" aria-hidden="true">
//         <use xlink:href="/assets/images/icons.svg#fill-alertes-aide"></use>
//       </svg>
//     </button>

//   </div>

// </body>
// </html>
