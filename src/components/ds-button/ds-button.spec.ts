import { newSpecPage } from '@stencil/core/testing';
import { Button } from './ds-button';

const colorProps = ["primary", "invert", "secondary", "tertiary", "info", "warning", "error"]
const sizeProps = ["small", "default", "large"];
const typeProps = ["button", "reset", "submit"];
const iconProps = ["square", "round"];

describe('ds-button', () => {
  // the most basic test, validate that component exists
  it('should build', () => {
    expect(new Button()).toBeTruthy();
  });

  // validate that render output is what it should be
  it('should render default version of button', async () => {
    const page = await newSpecPage({
      components: [Button],
      html: `<ds-button>Default</ds-button>`
    });
    expect(page.root).toEqualHtml(`
      <ds-button>
        <button class="default primary" type="button">Default</button>
      </ds-button>
    `);
  });

  // To test props etc., you can use loops instead of coding every test
  colorProps.forEach(color => {
    // and you can use the individual prop within the loop
    it('should render button with color: ' + color, async () => {
      const page = await newSpecPage({
        components: [Button],
        html: `<ds-button color="${color}">Default</ds-button>`
      });
      expect(page.root).toEqualHtml(`
        <ds-button color="${color}">
          <button class="default ${color}" type="button">Default</button>
        </ds-button>
      `);
    });
  })

  typeProps.forEach(type => {
    it('should render button with type: ' + type, async () => {
      const page = await newSpecPage({
        components: [Button],
        html: `<ds-button type="${type}">Default</ds-button>`
      });
      expect(page.root).toEqualHtml(`
        <ds-button type="${type}">
          <button class="primary default" type="${type}">Default</button>
        </ds-button>
      `);
    });
  })
  
  sizeProps.forEach(size => {
    it('should render button with size: ' + size, async () => {
      const page = await newSpecPage({
        components: [Button],
        html: `<ds-button size="${size}">Default</ds-button>`
      });
      expect(page.root).toEqualHtml(`
        <ds-button size="${size}">
          <button class="primary ${size}" type="button">Default</button>
        </ds-button>
      `);
    });
  })
  
  // the icon tests don't really make sense because of the svg, but put here as an example
  iconProps.forEach(icon => {
    it('should render button with leading icon: ' + icon, async () => {
      const page = await newSpecPage({
        components: [Button],
        html: `<ds-button icon-leading="${icon}">Default</ds-button>`
      });
      expect(page.root).toEqualHtml(`
        <ds-button icon-leading="${icon}">
          <button class="leading primary default" type="button">
          <svg
            class="icon"
            focusable="false"
            viewBox="0 0 32 32"
            aria-hidden="true"
          >
            <use xlink:href="/assets/images/icons.svg#fill-alertes-aide" />
          </svg>
          Default</button>
        </ds-button>
      `);
    });
  })
  
  iconProps.forEach(icon => {
    it('should render button with trailing icon: ' + icon, async () => {
      const page = await newSpecPage({
        components: [Button],
        html: `<ds-button icon-trailing="${icon}">Default</ds-button>`
      });
      expect(page.root).toEqualHtml(`
        <ds-button icon-trailing="${icon}">
          <button class="trailing primary default" type="button">
          Default
          <svg
            class="icon"
            focusable="false"
            viewBox="0 0 32 32"
            aria-hidden="true"
          >
            <use xlink:href="/assets/images/icons.svg#fill-alertes-aide" />
          </svg>
          </button>
        </ds-button>
      `);
    });
  })

});
